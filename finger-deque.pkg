# Deques implemented via finger trees

# Copyright (c) 2012 Michele Bini

# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
# BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
# ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# Requires: standard

# Mythryl's Queue API without type constructors, to encapsulate this
# implementation.

api Deque {

    Deque(X);

    empty_queue:     Deque(X);
    queue_is_empty:  Deque(X) -> Bool;

    push_item_onto_back_of_queue:    (Deque(X), X) -> Deque(X);
    push:			     (Deque(X), X) -> Deque(X);

    pull_item_off_front_of_queue:     Deque(X) -> (Deque(X), Null_Or(X));
    pull:			      Deque(X) -> (Deque(X), Null_Or(X));

    unpull_item_onto_front_of_queue: (Deque(X), X) -> Deque(X);
    unpull:			     (Deque(X), X) -> Deque(X);

    unpush_item_off_back_of_queue:   Deque(X) -> (Deque(X), Null_Or(X));
    unpush:			     Deque(X) -> (Deque(X), Null_Or(X));

    to_list:   Deque(X) -> List(X);
};

package finger_deque : Deque
{

    # Fingers are lists of trees of geometrically increasing size.

    # Trees in the finger are intermixed with items, to allow assembling
    # pairs of binary trees of the same height into perfect trees.
    #
    # Example:
    #
    #  	--x----a---y---b--
    #    / \   	  / \   	
    #
    # becomes:
    #
    #  	     --a---b--
    # 	      / \
    # 	     x 	 y
    #       / \ / \
    #

    package finger {
	Height = Int;

	package tree {
	    Tree(X) = EMPTY | NODE(Tree(X), X, Tree(X));

	    fun fold_left f i t = walk(t, i) where
		fun walk(EMPTY,         i) => i;
		    walk(NODE(a, b, c), i) => walk(a, f(b, walk(c, i)));
		end;
	    end;

	    fun fold_right f i t = walk(t, i) where
		fun walk(EMPTY,         i) => i;
		    walk(NODE(a, b, c), i) => walk(c, f(b, walk(a, i)));
		end;
	    end;
	};

	Finger(X) = List((X, Height, tree::Tree(X)));

	# These operations are not meant for exporting, but internally used by the to_list function
	fun fold_on_left_finger f i (t:Finger(X))   = list::fold_backward (fn ((i, h, t), b) = tree::fold_right f (f(i, b)) t) i t;
	fun fold_on_right_finger f i (t:Finger(X))  = list::fold_forward  (fn ((i, h, t), b) = tree::fold_right f (f(i, b)) t) i t;

	empty = NIL;

	# Push an element into the left side of the left finger
	fun lpush(finger, item) = try_join((item, 0, tree::EMPTY), finger)
	where
	    fun try_join(x, NIL) => [x];
		try_join(iht as (i,h,t), finger as (i',h',t')!r) => if (h == h')
		     # If there was already a tree of the same height in the finger, concatenate the current tree with the previous
		     try_join((i, h + 1, tree::NODE(t, i', t')), r);
		else
		     iht!finger;
		fi;
	    end;
	end;

	# Pop an element from the left side of the left finger
	fun lpop(NIL) => (NIL, NULL);
	    lpop((i, h, t)!r) => case (t)
		 tree::EMPTY => (r, THE(i));
		 tree::NODE(a, b, c) => {
		     h = h - 1;
		     # Split the current tree into two and try again
		     lpop((i, h, a)!(b, h, c)!r);
		 };
	    esac;
	end;

	# Push to right finger
	fun rpush(finger, item) = try_join((item, 0, tree::EMPTY), finger)
	where
	    fun try_join(x, NIL) => [x];
		try_join(iht as (i,h,t), finger as (i',h',t')!r) => if (h == h')
		     try_join((i', h + 1, tree::NODE(t', i, t)), r);
		else
		     iht!finger;
		fi;
	    end;
	end;

	# Pop from right finger
	fun rpop(NIL) => (NIL, NULL);
	    rpop((i, h, t)!r) => case (t)
		 tree::EMPTY => (r, THE(i));
		 tree::NODE(a, b, c) => {
		     h = h - 1;
		     rpop((b, h, c)!(i, h, a)!r);
		 };
	    esac;
	end;

    };

    Finger_Deque(X) = DEQUE {
	back:   finger::Finger(X),
	front:  finger::Finger(X)
    };

    empty = DEQUE {
	back   => NIL,
	front  => NIL
    };

    fun push(DEQUE { back, front }, item) = DEQUE { back => finger::lpush(back, item), front };

    fun unpush(deque as DEQUE { back, front }) = case (finger::lpop(back))
	 (back, NULL) => {
	     fun from_front(front, NIL, new_front) => case (finger::lpop([front]))
		      (back, item) =>
		      (DEQUE { back, front => list::reverse(new_front) }, item);
		 esac;
		 from_front(front, x!r, new_front) => from_front(x, r, front!new_front);
	     end;
	     case (front)
		  NIL => (deque, NULL);
		  x!r => from_front(x, r, NIL); # The left finger is empty: try to refill it with the biggest tree from the right finger
	     esac;
	 };
	 (back, item) => (DEQUE { back, front }, item);
    esac;

    fun unpull(DEQUE { back, front }, item) = DEQUE { front => finger::rpush(front, item), back };

    # Mirror of unpush algorithm
    fun pull(deque as DEQUE { back, front }) = case (finger::rpop(front))
	 (front, NULL) => {
	     fun from_back(back, NIL, new_back) => case (finger::rpop([back]))
		      (front, item) =>
		      (DEQUE { front, back => list::reverse(new_back) }, item);
		 esac;
		 from_back(back, x!r, new_back) => from_back(x, r, back!new_back);
	     end;
	     case (back)
		  NIL => (deque, NULL);
		  x!r => from_back(x, r, NIL);
	     esac;
	 };
	 (front, item) => (DEQUE { back, front }, item);
    esac;

    fun to_list(DEQUE { back, front }) = finger::fold_on_left_finger (!)
	(finger::fold_on_right_finger (!) NIL back) front;

    fun queue_is_empty(DEQUE { back => NIL, front => NIL }) => TRUE;
	queue_is_empty(_) => FALSE;
    end;

    # Compatibility to Mythryl's queue.api
    empty_queue = empty;
    push_item_onto_back_of_queue = push;
    unpull_item_onto_front_of_queue = unpull;
    pull_item_off_front_of_queue = pull;
    unpush_item_off_back_of_queue = unpush;
    Deque(X) = Finger_Deque(X);
};
